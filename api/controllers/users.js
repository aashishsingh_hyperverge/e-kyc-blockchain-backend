const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { validationResult } = require("express-validator");
const User = require("../models/users");
const Conversation = require("../models/Chat");
const institution = require("../models/institution");
const config = require("config");
const io = require('../db/io');
const networkConnection = require('../utils/networkConnection');


// console.log(UserSocket, "HELLO ABHISHEK");
// UserSocket["mango"] = "juice";
// console.log(UserSocket, "HELLO ABHISHEK1");

const { user_by_token } = require("./auth");

exports.user_register = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  console.log("111111")
  const { name, email, password,dob,id } = req.body;
  console.log("yayayayayay")
  try {
    let user = await User.findOne({ email });
    console.log(user);
    if (user) {
      return res.status(400).json({ errors: [{ msg: "User already exists" }] });
    }
    console.log("222",email)
    user = new User({
      name,
      email,
      password,
      dob,
      id
    });

    const salt = await bcrypt.genSalt(10);

    user.password = await bcrypt.hash(password, salt);
    console.log("33333")
    await user.save();
console.log("44444")
    const payload = {
      user: {
        id: user.id,
      },
    };

    jwt.sign(
      payload,
      config.get("jwtSecret"),
      { expiresIn: "5 days" },
      (err, token) => {
        if (err) throw err;
        res.json({ token });
      }
    );
    console.log("55555")
//trying stuff out
// const orgNum = req.orgNum;
const orgNum = "1";
// const ledgerUser = req.ledgerUser;
const ledgerUser="234";
const address="abc"
const clientData = JSON.stringify({ name, dob, address, id, whoRegistered: { orgNum, ledgerUser } });
console.log("cdata",clientData);
networkConnection
    .submitTransaction('createClient', orgNum, ledgerUser, [clientData])
    .then(async result => {
       await console.log("if res",result);
        if (result) {
            result = result.toString();
            console.log("result",result);
            if (result.length > 0) {
                await io.clientCreate(name, password, result, JSON.stringify({ orgNum, ledgerUser }));
                return res.json({ message: `New client ${result} created`, ledgerId: result });
            }
        }
        return res.status(500).json({ error: 'Something went wrong buddy' });
    })
    .catch((err) => {
        return res.status(500).json({ error: `Something went wrong bro\n ${err}` });
    });

//
  } catch (err) {
    log_and_send_error(err.message, 500, "Server Error");
  }
};

exports.user_register2 = async (req, res) => {
  // //const errors = validationResult(req);
  // if (!errors.isEmpty()) {
  //   return res.status(400).json({ errors: errors.array() });
  // }

  const { name, password } = req.body;
  console.log("111111")

  try {
    // let user = await User.findOne({  });

    // if (user) {
    //   return res.status(400).json({ errors: [{ msg: "User already exists" }] });
    // }

    user = new institution({
      name,
      password,
    });
    console.log("222222")

    const salt = await bcrypt.genSalt(10);

    user.password = await bcrypt.hash(password, salt);
    console.log("3333333");
    await user.save();
    console.log("444444")
    const payload = {
      user: {
        id: user.id,
      },
    };

    jwt.sign(
      payload,
      config.get("jwtSecret"),
      { expiresIn: "5 days" },
      (err, token) => {
        if (err) throw err;
        res.json({ token });
      }
    );
    console.log("5555555")
  } catch (err) {
    console.log(err);
  }
};


exports.getConversations = async (req, res) => {
  try {
    const { user_name } = req.body;
    let conversations = await Conversation.find({
      recipients: { $elemMatch: { $eq: req.user.id } },
    }).populate("recipients");

    res.status(200).send(conversations);
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server error");
  }
};

exports.newConversation = async (req, res) => {
  //console.log("inside route add conversation");
  //user_name:recipients
  //me:mera user
  const { user_name } = req.body;
  //console.log(user_name);
  try {
    let otherUser = await User.findOne({ name: user_name }).select("-password");

    if (!otherUser) {
      res.status(404).send("This user does not exist");
    }
    //console.log(otherUser);
    let oldConvo = await Conversation.findOne({
      recipients: [req.user.id, otherUser.id],
    }).populate("recipients");

    if (oldConvo) {
      //console.log("inside route add conversation hello");
      res.status(200).send(oldConvo);
    } else {
      //console.log("inside route add conversation old");
      let newConvo = new Conversation({
        recipients: [req.user.id, otherUser.id],
      });
      //console.log("inside route add conversation2");
      await newConvo.save();
      newConvo = await Conversation.findOne({
        recipients: [req.user.id, otherUser.id],
      }).populate("recipients");
      //const user = User.findById(req.user.id);
      //console.log(req.socket.id, "my socket");
      // console.log(req.user.name, "my name");
      // console.log(user_name, "recipient");

      //prev one
      // console.log(UserSocket[user.name].id, "@@@@@@@");
      // await UserSocket[user.name].join(newConvo._id);

      // console.log(UserSocket[user.name].id, "my id ");
      // console.log(UserSocket[user_name], "OP");

      // if (UserSocket[user_name] !== undefined) {
      //   console.log(UserSocket[user_name], user_name, "s socket");
      //   await UserSocket[user_name].join(newConvo._id);
      //   console.log("emitiing addConversation", newConvo._id);
      //   await UserSocket[user.name]
      //     .in(newConvo._id)
      //     .emit("newConversation", { newConvo });
      // }
      // await res.status(200).send(newConvo);

      //new one using req
      
    console.log(req.socket.id,UserSocket[user_name].id,'test1')
     
    await req.socket.join(newConvo._id);
      
      
      if (UserSocket[user_name] !== undefined) {
        await UserSocket[user_name].join(newConvo._id);
        await req.socket.in(newConvo._id).emit("newConversation", { newConvo });
      }
      
      
      await res.status(200).send(newConvo);



    }
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server error");
  }
};

exports.getChatById = async (req, res) => {
  const { chatid } = req.body;

  try {
    const chat = await Conversation.findById(chatid).populate("recipients");

    res  .send(chat);
  } catch (err) {
    console.log("Server Error" + err);
  }
};